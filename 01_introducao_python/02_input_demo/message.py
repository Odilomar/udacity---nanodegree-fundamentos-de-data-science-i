names = input("Enter names separeted by commas: ") # capture e processe o input para uma lista de nomes
assignments = input("Enter assignment counts separated by commas: ") # capture e processe o input para uma lista do número de tarefas
grades = input("Enter grades separated by commas: ") # capture e processe o input para uma lista de notas

names = names.split(",")
assignments = assignments.split(",")
grades = grades.split(",")

# string de mensagem a ser usada para cada aluno
# DICA: use .format() com esta string no seu loop for
message = "Hi {},\n\nThis is a reminder that you have {} assignments left to \
submit before you can graduate. You're current grade is {} and can increase \
to {} if you submit all assignments before the due date.\n\n"

# escreva um loop for que realize uma iteração em cada conjunto de nomes, tarefas e notas para imprimir a mensagem de cada aluno
for i in range(len(names)):
    print(message.format(names[i].title(), assignments[i].title(), grades[i].title(), int(assignments[i])*2 + int(grades[i])))