# coding: utf-8

# Melhor tempo de execução: 21.08 segundos

# Udacity - Nanodegree Fundamentos de Data Science I
# Aluno: Odilomar Rebelo Rocha Júnior
# Email: odilomar.junior@gmail.com
# Telefone: (92) 99423 - 0977 / 99968 - 4041

print("Importando bibliotecas...")
import csv
import matplotlib.pyplot as plt


def read_file(filename: str) -> list:
    """
        Função que ler o arquivo.
        Argumento:
            filename: é uma string que contem o nome do arquivo.
        Retorna: uma lista contendo todas as informações do arquivo.
    """

    # Inicialização da variável
    data = []

    # Acesso, leitura e cópia do arquivo para a variável "data"
    with open(filename, "r") as file_read:
        reader = csv.reader(file_read)
        data = list(reader)

    # Retona a variável
    return data


def first_task(data_list: list):
    """
        Função responsável por imprimir as 20 primeiras linhas da lista.
        Argumento:
            data_list: é a lista com as informações.
        Retorna: None
    """

    for index in range(20):
        print("Linha {}: {}".format(index + 1, data_list[index]))


def second_task(data_list: list):
    """
        Função responsável por imprimir o gênero das 20 primeiras linhas.
        Argumento:
            data_list: é a lista com as informações.
        Retorna: None
    """

    for index in range(20):
        print("Linha {}: {}".format(index + 1, data_list[index][6]))


def column_to_list(data: list, index: int = 0) -> list:
    """
        Função que transforma uma coluna de uma lista em uma lista.
        Argumentos:
            data: é a lista de onde a coluna será retirada.
            index: é o indice referente a coluna da lista a ser removida. (default 0).
        Retorna: uma lista contendo os elementos da coluna index.
    """

    # Inicializa a lista
    column_list = []

    # Dica: Você pode usar um for para iterar sobre as amostras, pegar a feature pelo seu índice, e dar append para uma lista
    for info in data:
        column_list.append(info[index])

    # Retorna a lista
    return column_list


def count_gender(data_list: list) -> list:
    """
        Função que conta os gêneros.
        Argumento:
            data_list: lista onde será realizada a busca e contagem dos gêneros.
        Retorna: uma lista com as quantidades de cada gênero.
    """
    # Inicializa das variáveis
    male = 0
    female = 0

    # Realiza a busca e a contagem dos gêneros
    for data in data_list:
        if data[-2] == "Male":
            male += 1
        elif data[-2] == "Female":
            female += 1

    # Retorna os valores dos gêneros como uma lista
    return [male, female]


def most_popular_gender(data_list: list) -> str:
    """
        Função que pega o gênero mais popular.
        Argumento:
            data_list: lista que contem as informações a serem calculadas.
        Retorna: uma string com os valores "Male", "Female", ou "Equal".
    """

    # Inicialização das variáveis locais
    answer = ""
    male, female = count_gender(data_list)

    # Utiliza as variáveis anteriores para calcular o gênero
    if male > female:
        answer = "Male"
    elif male < female:
        answer = "Female"
    else:
        answer = "Equal"

    # Retorna o gênero mais popular
    return answer


def seventh_task(data_list: list):
    """
        Função responsável por criar o gráfico e apresentar ao usuário.
        Argumento:
            data_list: é a lista que contem as informações .
        Retorna: None.
    """

    # Cria os elementos do eixo x e y
    x, y = count_items(column_to_list(data_list, -3))

    # Plota os valores anteriores nos seus devidos eixos em forma de barra
    plt.bar(x, y)

    # Define as descrições dos eixos x e y
    plt.xlabel("Tipo de usuário")
    plt.ylabel("Quantidade")

    # Define o título do gráfico
    plt.title("Quantidade por gênero")

    # Apresentao gráfico para o usuário
    plt.show()


def ninth_task(trip_duration_list: list) -> tuple:
    """
        Função responsável por calcular o valor mínimo, máximo, médio e mediano da lista de informações
        Argumento:
            trip_duration_list: é a lista de informações que contem os dados referente a duração do percurso em segundos.
        Retorna: uma tupla contendo o valor mínimo (int), o valor máximo (int), o valor médio (float) e a mediana (int).
    """
    # Inicializa as variáveis
    min_trip = 0.
    max_trip = 0.
    mean_trip = 0.
    median_trip = 0.

    # Orneda a lista
    print("\nOrdenando lista da duração da viagem...")
    trip_duration_list.sort(key=lambda trip: int(trip))

    # Atribui o menor e o maior valor da lista para as variáveis
    min_trip = int(trip_duration_list[0])
    max_trip = int(trip_duration_list[-1])

    # Soma os valores e calcula a média dos valores
    for trip in trip_duration_list:
        mean_trip += int(trip)

    mean_trip = round(mean_trip / len(trip_duration_list))

    # Calcula a mediana
    if len(trip_duration_list) % 2 != 0:
        median_trip = int(trip_duration_list[int(
            int(len(trip_duration_list) + 1) / 2)])
    else:
        median_trip = int(
            int(trip_duration_list[int(int(len(trip_duration_list)) / 2)]) +
            int(trip_duration_list[int(int(len(trip_duration_list) + 1) /
                                       2)]) / 2)

    # Retorna uma tupla
    return min_trip, max_trip, mean_trip, median_trip


def count_items(column_list) -> tuple:
    """
        Função responsável por contabilizar os tipos de usuários e a quantidade de vezes que cada um deles aparece.
        Argumento:
            column_list: é a lista que representa a coluna dos tipos de usuários.
        Retorna: uma tupla contendo uma lista dos tipos de itens e uma lista da quantidade de cada item.
    """
    # Inicializa as variáveis
    item_types = []
    count_items = []

    # Agrupa os tipos diferentes de usuário e insere na variável "item_types" como uma lista
    item_types = list(set(column_list))

    # Inicializa as possições da variável "count_items" que serão utilizados no próximo loop
    for i in range(len(item_types)):
        count_items.append(0)

    # Calcula a quantidade de vezes um tipo de usuário aparece na variável "column_list"
    # e soma um no seu respectivo indice da lista "count_items"
    for data in column_list:
        if data in item_types:
            count_items[item_types.index(data)] += 1

    # Retorna uma tupla com as listas "item_types" e "count_items"
    return item_types, count_items


def main():
    """
        Função responsável pelo escopo principal do código.
        Retorna: None.
    """

    # Inicialização das variáveis do sistema
    data_list = []
    filename = "chicago.csv"
    male = 0
    female = 0

    # Vamos ler os dados como uma lista
    print("Lendo o documento...")
    data_list = read_file(filename)
    print("\nOk!")

    # Vamos verificar quantas linhas nós temos
    print("Número de linhas importadas: {}".format(len(data_list)))

    input("Aperte Enter para continuar...")
    # TAREFA 1
    # TODO: Imprima as primeiras 20 linhas usando um loop para identificar os dados.
    print("\n\nTAREFA 1: Imprimindo as primeiras 20 amostras")

    # Vamos mudar o data_list para remover o cabeçalho dele.
    data_list = data_list[1:]

    first_task(data_list)

    # Nós podemos acessar as features pelo índice
    # Por exemplo: sample[6] para imprimir gênero, ou sample[-2]

    input("Aperte Enter para continuar...")
    # TAREFA 2
    # TODO: Imprima o `gênero` das primeiras 20 linhas

    print("\nTAREFA 2: Imprimindo o gênero das primeiras 20 amostras")

    second_task(data_list)

    # Ótimo! Nós podemos pegar as linhas(samples) iterando com um for, e as colunas(features) por índices.
    # Mas ainda é difícil pegar uma coluna em uma lista. Exemplo: Lista com todos os gêneros

    input("Aperte Enter para continuar...")
    # TAREFA 3
    # TODO: Crie uma função para adicionar as colunas(features) de uma lista em outra lista, na mesma ordem
    # Vamos checar com os gêneros se isso está funcionando (apenas para os primeiros 20)
    print(
        "\nTAREFA 3: Imprimindo a lista de gêneros das primeiras 20 amostras")
    print(column_to_list(data_list, -2)[:20])

    # ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
    assert type(
        column_to_list(data_list, -2)
    ) is list, "TAREFA 3: Tipo incorreto retornado. Deveria ser uma lista."
    assert len(column_to_list(
        data_list, -2)) == 1551505, "TAREFA 3: Tamanho incorreto retornado."
    assert column_to_list(data_list, -2)[0] == "" and column_to_list(
        data_list, -2)[1] == "Male", "TAREFA 3: A lista não coincide."
    # -----------------------------------------------------

    input("Aperte Enter para continuar...")
    # Agora sabemos como acessar as features, vamos contar quantos Male (Masculinos) e Female (Femininos) o dataset tem
    # TAREFA 4
    # TODO: Conte cada gênero. Você não deveria usar uma função para isso.

    # Realiza a busca e a contagem dos gêneros
    male, female = count_gender(data_list)

    # Verificando o resultado
    print(
        "\nTAREFA 4: Imprimindo quantos masculinos e femininos nós encontramos"
    )
    print("Masculinos: ", male, "\nFemininos: ", female)

    # ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
    assert male == 935854 and female == 298784, "TAREFA 4: A conta não bate."
    # -----------------------------------------------------

    input("Aperte Enter para continuar...")
    # Por que nós não criamos uma função para isso?
    # TAREFA 5
    # TODO: Crie uma função para contar os gêneros. Retorne uma lista.
    # Isso deveria retornar uma lista com [count_male, count_female] (exemplo: [10, 15] significa 10 Masculinos, 15 Femininos)

    print("\nTAREFA 5: Imprimindo o resultado de count_gender")
    print(count_gender(data_list))

    # ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
    assert type(
        count_gender(data_list)
    ) is list, "TAREFA 5: Tipo incorreto retornado. Deveria retornar uma lista."
    assert len(
        count_gender(data_list)) == 2, "TAREFA 5: Tamanho incorreto retornado."
    assert count_gender(data_list)[0] == 935854 and count_gender(
        data_list)[1] == 298784, "TAREFA 5: Resultado incorreto no retorno!"
    # -----------------------------------------------------

    input("Aperte Enter para continuar...")
    # Agora que nós podemos contar os usuários, qual gênero é mais prevalente?
    # TAREFA 6
    # TODO: Crie uma função que pegue o gênero mais popular, e retorne este gênero como uma string.
    # Esperamos ver "Male", "Female", ou "Equal" como resposta.

    print("\nTAREFA 6: Qual é o gênero mais popular na lista?")
    print("O gênero mais popular na lista é: {} ".format(
        most_popular_gender(data_list)))

    # ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
    assert type(
        most_popular_gender(data_list)
    ) is str, "TAREFA 6: Tipo incorreto no retorno. Deveria retornar uma string."
    assert most_popular_gender(
        data_list) == "Male", "TAREFA 6: Resultado de retorno incorreto!"
    # -----------------------------------------------------

    input("Aperte Enter para continuar...")
    # TAREFA 7
    # TODO: Crie um gráfico similar para user_types. Tenha certeza que a legenda está correta.
    print("\nTAREFA 7: Verifique o gráfico!")

    seventh_task(data_list)

    input("Aperte Enter para continuar...")
    # TAREFA 8
    # TODO: Responda a seguinte questão
    male, female = count_gender(data_list)
    print("\nTAREFA 8: Por que a condição a seguir é Falsa?")
    print("male + female == len(data_list):", male + female == len(data_list))
    answer = "Pois não estão sendo contabilizados as empresas que disponibilizam as bicicletas (Customer)"
    print("resposta:", answer)

    # ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
    assert answer != "Escreva sua resposta aqui.", "TAREFA 8: Escreva sua própria resposta!"
    # -----------------------------------------------------

    input("Aperte Enter para continuar...")
    # Vamos trabalhar com trip_duration (duração da viagem) agora. Não conseguimos tirar alguns valores dele.
    # TAREFA 9
    # TODO: Ache a duração de viagem Mínima, Máxima, Média, e Mediana.
    # Você não deve usar funções prontas para isso, como max() e min().
    trip_duration_list = column_to_list(data_list, 2)

    min_trip, max_trip, mean_trip, median_trip = ninth_task(trip_duration_list)

    print("\nTAREFA 9: Imprimindo o mínimo, máximo, média, e mediana")
    print("Min: ", min_trip, "Max: ", max_trip, "Média: ", mean_trip,
          "Mediana: ", median_trip)

    # ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
    assert round(min_trip) == 60, "TAREFA 9: min_trip com resultado errado!"
    assert round(max_trip) == 86338, "TAREFA 9: max_trip com resultado errado!"
    assert round(mean_trip) == 940, "TAREFA 9: mean_trip com resultado errado!"
    assert round(
        median_trip) == 670, "TAREFA 9: median_trip com resultado errado!"
    # -----------------------------------------------------

    input("Aperte Enter para continuar...")
    # TAREFA 10
    # Gênero é fácil porque nós temos apenas algumas opções. E quanto a start_stations? Quantas opções ele tem?
    # TODO: Verifique quantos tipos de start_stations nós temos, usando set()
    start_stations = set(column_to_list(data_list, 3))

    print("\nTAREFA 10: Imprimindo as start stations:")
    print(len(start_stations))
    print(start_stations)

    # ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
    assert len(start_stations
               ) == 582, "TAREFA 10: Comprimento errado de start stations."
    # -----------------------------------------------------

    input("Aperte Enter para continuar...")
    # TAREFA 11
    # Volte e tenha certeza que você documentou suas funções. Explique os parâmetros de entrada, a saída, e o que a função faz. Exemplo:
    # def new_function(param1: int, param2: str) -> list:
    """
        Função de exemplo com anotações. 
        Argumentos: 
            param1: O primeiro parâmetro. 
            param2: O segundo parâmetro. 
        Retorna: Uma lista de valores x. 
    """

    print("\nTAREFA 11: Documentação das funções realizadas")

    input("Aperte Enter para continuar...")
    # TAREFA 12 - Desafio! (Opcional)
    # TODO: Crie uma função para contar tipos de usuários, sem definir os tipos
    # para que nós possamos usar essa função com outra categoria de dados.

    # ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
    column_list = column_to_list(data_list, -2)
    types, counts = count_items(column_list)
    print("\nTAREFA 12: Imprimindo resultados para count_items()")
    print("Tipos:", types, "Counts:", counts)
    assert len(types) == 3, "TAREFA 12: Há 3 tipos de gênero!"
    assert sum(counts) == 1551505, "TAREFA 12: Resultado de retorno incorreto!"
    # -----------------------------------------------------


if __name__ == "__main__":
    main()